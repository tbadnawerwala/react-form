export const change = (formData)=>{
    return {
        type: 'CHANGE',
        payload: formData
    }
};

export const back = (formData)=>{
    return {
        type: 'BACK',
        payload: formData
    }
};
