import { takeEvery, put, delay } from "redux-saga/effects";

function* changeAsync(action) {
    yield delay(400);
    yield put({ type: "CHANGE_ASYNC", payload: action.payload });
  }
  
  export function* changeSaga() {
    yield takeEvery("CHANGE", changeAsync);
  }