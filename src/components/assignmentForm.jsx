import React, { useState, useMemo, useCallback, useEffect  } from "react";
import debounce from "lodash.debounce";
import TextField from "./TextField";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Container, Grid, Paper } from "@material-ui/core";
import {useStoreContext} from '../contextApi/formContext'; // for using context api
import { addData } from '../contextApi/contextReducer';
import {
  singleFieldValidation,
  allFieldsValidation,
} from "../utils/validation";
import FormResult from "./formResult";


const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  container: {
    padding: theme.spacing(3),
  },
}));
const waitTime = 1000;

const AssignmentForm = () => {

  
  const [formData1, dispatch] = useStoreContext(); 
  let formData = formData1;
  const classes = useStyles();
  const paperStyle = {
    padding: 20,
    width: 280,
    margin: "20px auto",
  };
  const containStyle = { display: "block", overflow: "auto" };
  const [errorData, setError] = useState({});
  const [isSubmitted, setSubmitted] = useState(false);
  const [isEdit, setEdit] = useState(false);

  if (isSubmitted) {
    formData = { fullName: "", address: "", mobileNumber: "", college: "" };
  }

  useEffect(() => {
    setSubmitted(false);
  }, []);

  const onChange1 = useCallback((event) => {
    setEdit(false);
    const { name, value } = event.target;
    setSubmitted(false);
    debounceSingleFieldValidation({ name, value });
    Object.keys(formData).map((item) =>
      item === name ? (formData[item] = value) : ""
    );
   dispatch(addData(formData))
  }, []);
  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      const { isValid, errors } = allFieldsValidation(formData);
      if (!isValid) {
        setError(errors);
      } else {
        setSubmitted(true);
        formData = {
          fullName: "",
          address: "",
          mobileNumber: "",
          college: "",
        };
      }
    },
    [formData]
  );

  const debounceSingleFieldValidation = useMemo(
    () =>
      debounce(({ name, value }) => {
        const { formErrors } = errorData;
        const { isValid, errors } = singleFieldValidation({ key: name, value });
        if (!isValid) {
          setError({ [name]: errors[name] });
        } else {
          setError({ [name]: null });
        }
      }, waitTime),
    [onChange1]
  );

  const editData = useCallback((data) => {
    formData = {
      fullName: data.fullName,
      address: data.address,
      mobileNumber: data.mobileNumber,
      college: data.college,
    };
    setSubmitted(false);
    setEdit(true);
  }, []);

  const temp = useMemo(() => {
    if (isSubmitted || isEdit) {
      return <FormResult editData={editData} isSubmitted={isSubmitted} />;
    } else {
      return;
    }
  }, [isSubmitted, isEdit]);
  return (
    <Container style={containStyle}>
      <form noValidate onSubmit={handleSubmit}>
        <Grid container spacing={3}>
          <Paper elevation={10} style={paperStyle}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    error={errorData["fullName"]}
                    //onChange={(e) => onChange(e)}
                    onChange={onChange1}
                    value={formData.fullName}
                    name="fullName"
                    title="Name"
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    error={errorData["address"]}
                    onChange={onChange1}
                    value={formData.address}
                    name="address"
                    title="Address"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={errorData["mobileNumber"]}
                    onChange={onChange1}
                    value={formData.mobileNumber}
                    name="mobileNumber"
                    title="Mobile Number"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={errorData["college"]}
                    onChange={onChange1}
                    value={formData.college}
                    name="college"
                    title="College"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} style={{ marginTop: 20 }}>
              <Button color="primary" type="submit" variant="contained">
                Add/Update
              </Button>
            </Grid>
          </Paper>
        </Grid>
      </form>
      {temp}
    </Container>
  );
};

export default AssignmentForm;
