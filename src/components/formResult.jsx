import React from "react";
import { Container, Grid, Paper } from "@material-ui/core";
import {useStoreContext} from '../contextApi/formContext'; 

const FormResult = (props) => {
    const [formData, dispatch] = useStoreContext()
  const paperStyle = {
    padding: 20,
    height: "70vh",
    width: 280,
    margin: "20px auto",
  };

  const { fullName, address, mobileNumber, college } = formData;

  return (
    <div>
      <Grid>
        <Paper elevation={10} style={paperStyle}>
          <div>
            <h2>Filled Details</h2>
          </div>
          <div class="form-group">
            <label>Name:</label>&nbsp;
            {fullName}
          </div>
          <div class="form-group">
            <label>Address:</label>&nbsp;
            {address}
          </div>
          <div class="form-group">
            <label>Mobile Number:</label>&nbsp;
            {mobileNumber}
          </div>
          <div class="form-group">
            <label>College:</label>&nbsp;
            {college}
          </div>
          <div>
            <button
              className="btn btn-secondary"
              type="button"
              onClick={() => {
                props.editData(formData);
              }}
            >
              Edit
            </button>
          </div>
        </Paper>
      </Grid>
    </div>
  );
};
export default FormResult;
