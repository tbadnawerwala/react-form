import {combineReducers} from 'redux';
import assignmentReducer from './assignmentFormReducer';

const allReducers = combineReducers({
    formDetails: assignmentReducer
})

export default allReducers;