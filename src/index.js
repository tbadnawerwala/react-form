import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { FormDataProvider } from './contextApi/formContext';
import { initialState, formDataReducer } from './contextApi/contextReducer';

ReactDOM.render(
  <React.StrictMode>
    <FormDataProvider in initialState={initialState} reducer={formDataReducer}>
      <App />
    </FormDataProvider>
    
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
