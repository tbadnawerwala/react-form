import React,{useState, createContext, useReducer, useContext} from 'react';

export const FormContext = createContext();
export const useStoreContext = () => useContext(FormContext); //creating our own useContext hook

export const FormDataProvider = ({children, initialState, reducer}) =>{

    // initilize all the state data here
    
    const [globalState, dispatch] = useReducer(reducer, initialState)
    console.log("global state", globalState)
    return (
        <FormContext.Provider value={[globalState, dispatch]}>{children}</FormContext.Provider>
    );
}